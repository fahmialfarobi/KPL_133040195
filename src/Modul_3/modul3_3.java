/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul_3;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tya Wahyuni
 */
public class modul3_3 {
//    private static Throwable throwable;
//private tiga() throws Throwable {
//throw throwable;
//}
//public static synchronized void undeclaredThrow(Throwable throwable) {
//// These exceptions should not be passed
//if (throwable instanceof IllegalAccessException ||
//throwable instanceof InstantiationException) {
//// Unchecked, no declaration required
//throw new IllegalArgumentException();
//}
//tiga.throwable = throwable;
//try {
//// Next line throws the Throwable argument passed in above,
//// even though the throws clause of class.newInstance fails
//// to declare that this may happen
//tiga.class.newInstance();
//} catch (InstantiationException e) { /* Unreachable */
//} catch (IllegalAccessException e) { /* Unreachable */
//} finally { // Avoid memory leak
//tiga.throwable = null;
//}
//}
    private static Throwable throwable;
private modul3_3() throws Throwable {
throw throwable;
}
    public static synchronized void undeclaredThrow(Throwable throwable) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
// These exceptions should not be passed
if (throwable instanceof IllegalAccessException ||
throwable instanceof InstantiationException) {
// Unchecked, no declaration required
throw new IllegalArgumentException();
}
modul3_3.throwable = throwable;
try {
Constructor constructor =
modul3_3.class.getConstructor(new Class<?>[0]);
constructor.newInstance();
} catch (InstantiationException e) { /* Unreachable */
} catch (IllegalAccessException e) { /* Unreachable */
} catch (InvocationTargetException e) {
System.out.println("Exception thrown: "
+ e.getCause().toString());
} finally { // Avoid memory leak
modul3_3.throwable = null;
}
}
public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        try {
            // No declared checked exceptions
            modul3_3.undeclaredThrow(
                    new Exception("Any checked exception"));
        } catch (InstantiationException ex) {
            Logger.getLogger(modul3_3.class.getName()).log(Level.SEVERE, null, ex);
        }
    System.out.println(modul3_3.throwable);
}
}



